def record_in_db(topic, message):
    topiclist = topic.split("/")
    name = topiclist[len(topiclist)-2]
    #check if it's the first message sent by this device
    queryResult = db.session.query(Device).filter(Device.name == name).all()
    if not queryResult:
        register_new_device(name)
    #once we are certain the device is registered into the db, get its sensors' IDs
    deviceID = db.session.query(Device).filter(Device.name == name).all()[0].id
    reading = json.loads(message[2:-1])
    timestamp = reading['Time']
    timestamp = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S')
    temperature = reading['AM2301']['Temperature']
    humidity = reading['AM2301']['Humidity']
    tempSensorID = db.session.query(Sensor).filter(Sensor.device_id == deviceID).\
    filter(Sensor.type == 'Temperature').all()[0].id
    humSensorID = db.session.query(Sensor).filter(Sensor.device_id == deviceID).\
    filter(Sensor.type == 'Humidity').all()[0].id
    #when we have IDs for both sensors, add new readings to db
    db.session.add(Reading(timestamp, temperature, tempSensorID))
    db.session.add(Reading(timestamp, humidity, humSensorID))
    db.session.commit()