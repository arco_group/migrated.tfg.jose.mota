@app.route('/metadata/add/sensor', methods=['POST'])
def sensAdd():
    name = request.form['sname']
    units = request.form['sunits']
    min = request.form['smin']
    max = request.form['smax']
    sensor = Sensor.query.filter_by(name = name).first()
    sensor.units = None if units == '' else units
    sensor.min = None if min == '' else float(min)
    sensor.max = None if max == '' else float(max)
    db.session.commit()
    return redirect(url_for("editor", last_edited = name))

@app.route('/metadata/add/device', methods=['POST'])
def devAdd():
    name = request.form['dname']
    location = request.form['dlocation']
    brand = request.form['dbrand']
    model = request.form['dmodel']
    device = Device.query.filter_by(name = name).first()
    device.location = None if location == '' else location
    device.brand = None if brand == '' else brand
    device.model = None if model == '' else model
    db.session.commit()
    return redirect(url_for("editor", last_edited = name))