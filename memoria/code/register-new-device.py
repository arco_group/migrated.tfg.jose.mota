def register_new_device(name):
    db.session.add(Device(name))
    db.session.commit()
    deviceID = db.session.query(Device).filter(Device.name == name).all()[0].id
    tempSensorName="Temp"+name[-4:]
    humSensorName="Humi"+name[-4:]
    db.session.add(Sensor(tempSensorName,'Temperature',deviceID))
    db.session.add(Sensor(humSensorName,'Humidity',deviceID))
    db.session.commit()