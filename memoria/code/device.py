class Device(db.Model):
    __tablename__ = 'device'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    brand = db.Column(db.String)
    model = db.Column(db.String)
    location = db.Column(db.String)
    sensors = db.relationship('Sensor', backref='device', lazy='dynamic')