class Sensor(db.Model):
    __tablename__ = 'sensor'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    type = db.Column(db.String)
    units = db.Column(db.String)
    max = db.Column(db.Float)
    min = db.Column(db.Float)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
    readings = db.relationship('Reading', backref='sensor', lazy='dynamic')