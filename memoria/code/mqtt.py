def mqtt_subscribe(topic):
    def on_connect(client, userdata, rc):
        client.subscribe(topic)

    def on_message(client, userdata, msg):
        record_in_db(msg.topic, str(msg.payload))

    client = mqtt.Client("ESI_DashBoard")
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("localhost", 1883, 60)
    client.loop_start()