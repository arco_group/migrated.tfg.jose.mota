class Reading(db.Model):
    __tablename__ = 'reading'
    timestamp = db.Column(db.TIMESTAMP(timezone=True), primary_key=True)
    value = db.Column(db.Float)
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'), primary_key=True)