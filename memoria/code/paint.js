function paint()
{
  var sensor=document.querySelector('[name="sensor"]').options[document.querySelector('[name="sensor"]').selectedIndex].value;
  var data_requested= document.querySelector('[name="req"]:checked').value;
  d3.json("php/data.php?sensor="+sensor+"&data_requested="+data_requested, function(error, data) {
  chart_gauge(data[0][data_requested],data_requested)
  chart(data,data_requested)
  });
}