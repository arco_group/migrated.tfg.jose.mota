function loadDeviceData(device_name)
{
    document.getElementById("uneditable").innerHTML ='';
    loadDeviceForm(device_name,true);
}

function loadSensorData(sensor_name)
{
    loadSensorForm(sensor_name);
}

function loadDeviceForm(device_name,editable) {
    var json_obj = JSON.parse(Get("/metadata/device/"+device_name));

    if(editable)
        document.getElementById("editable").innerHTML=
            ...
    else
        document.getElementById("uneditable").innerHTML=
            ...
}

function loadSensorForm(sensor_name) {
    var json_obj = JSON.parse(Get("/metadata/sensor/"+sensor_name));
    loadDeviceForm(json_obj.metadata[0].name,false)
    document.getElementById("editable").innerHTML= 
    ...
}