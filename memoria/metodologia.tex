\chapter{Metodología y herramientas}
\label{chap:metodologia}

\drop{E}{ste} capítulo pretende mostrar el flujo de trabajo que se desea seguir durante el desarrollo de este proyecto. Se intentará dar una serie de nociones teóricas generales de la metodología empleada y de cómo se aplicará al proyecto, así como de las herramientas software y hardware de las que se hará uso.

\section{Metodología}
Con el fin de desarrollar este proyecto se han tenido en cuenta diferentes metodologías ágiles así como tradicionales.

Las metodologías ágiles, según el «Manifiesto por el desarrollo ágil del software» \cite{beck2001manifiesto} tienen los siguientes valores:
\begin{quote}
	«Estamos descubriendo formas mejores de desarrollar software tanto por nuestra propia experiencia como ayudando a terceros. A través de este 		trabajo hemos aprendido a valorar:
	\begin{itemize}
		\item Individuos e interacciones sobre procesos y herramientas.
		\item Software funcionando sobre documentación extensiva.
		\item Colaboración con el cliente sobre negociación contractual.
		\item Respuesta ante el cambio sobre seguir un plan.
	\end{itemize}
	Esto es, aunque valoramos los elementos de la derecha, valoramos más los de la izquierda.»
\end{quote}
Este tipo de metodologías son indicadas para proyectos en los que no se tienen claros todos los requisitos al comienzo, o en los que se necesita retroalimentación por parte del cliente, puesto que su flexibilidad es su punto fuerte y permiten la adaptación del desarrollo del proyecto frente al cambio.

Las metodologías tradicionales se centran más en la planificación del proyecto y el seguimiento de dicha planificación, así como en una especificación de requisitos y un modelado precisos. Este tipo de metodologías son menos flexibles, pero ayudan enormemente al desarrollo cuando se conocen bien los requisitos y se sabe que no habrá cambios durante el proyecto. También son utilizadas cuando se tiene poca experiencia, debido a su sencillez y linealidad.

Como para el diseño y despliegue de la infraestructura de red se tienen muy claros los requisitos y no se puede iterar de forma sencilla tras haberla desplegado, para llevar a cabo su desarrollo se ha decidido seguir la metodología descrita en «Designing and Deploying 802.11 Wireless Networks», el cual propone un modelo en cascada compuesto por una fase de análisis de requisitos, una fase de diseño, una fase de implementación y una fase de operaciones y mantenimiento.

A continuación se describen brevemente las fases de la metodología recomendada en~\cite{Geier15}:
\begin{itemize}
	\item Análisis de requisitos.\\
		Análisis y evaluación de las necesidades de los usuarios de la red y los sistemas ya existentes. En esta fase se generará un documento que 		describa todos los requisitos que deban cumplirse en el nuevo sistema, para ello se realizarán las siguientes acciones:
		\begin{enumerate}
			\item Obtención de un listado con las necesidades de los usuarios y la información del sistema existente.
			\item Análisis de los requisitos y agregación los que se consideren necesarios aparte de los recopilados.
			\item Documentación de los requisitos y obtención de la aprobación de los mismos por parte de los interesados.
		\end{enumerate}

	\item Diseño.\\
		En esta fase se determina el grado en el que se satisfarán los requisitos anteriormente mencionados, e incluye la definición de la 					arquitectura, ubicaciones óptimas para los puntos de acceso, información sobre las configuraciones, etc. 
		Para obtener dicha información, se llevarán a cabo las siguientes tareas:
		\begin{enumerate}
			\item Definición de la arquitectura del sistema.
			\item Selección de productos hardware a utilizar en la implementación.
			\item Inspección inalámbrica del lugar.
			\item Definición de la configuración de los componentes hardware.
			\item Verificación y documentación del diseño, y obtención de la aprobación.
		\end{enumerate}
		
	\item Implementación.\\
		Durante la fase de implementación se llevará a cabo la adquisición de los componentes, así como el desarrollo de la aplicación software de 		muestra. La  parte final de esta fase consiste en la realización de pruebas de verificación. Las tareas que se llevarán a cabo en esta 				fase serán:
		\begin{enumerate}
			\item Adquisición del hardware necesario.
			\item Desarrollo de la aplicación de muestra.
			\item Configuración e instalación de un elemento piloto con el fin de comprobar su funcionamiento así como el de la aplicación, de 						  modo que si es necesario realizar algún cambio, sea menos costoso que aplicar el cambio a la instalación completa.
			\item Instalación completa del sistema y realización de pruebas.
			\item Documentación de todo el proceso de instalación.
		\end{enumerate}

	\item Operaciones y mantenimiento.\\
		Aquí se incluyen distintas funciones, desde administración y monitorización del sistema hasta formación de los usuarios, pasando por 				mantenimiento y resolución de problemas. Para ello es necesario:
		\begin{enumerate}
			\item Definición de procedimientos de soporte, y preparación del personal que lo llevará a cabo.
			\item Formación de los usuarios en caso de ser necesaria.
			\item Transferencia de la red al departamento de operaciones y mantenimiento.
		\end{enumerate}			
\end{itemize}
Una vez finalizada la fase de operaciones y mantenimiento, el sistema se encontrará en producción y se considerará entregado el proyecto.

Cabe mencionar que, aunque el desarrollo de la aplicación de muestra forme parte de la fase de implementación, la metodología que se utilizará para su desarrollo no será la metodología en cascada, puesto que para esta no se encuentran bien definidos los requisitos. En su lugar se empleará una metodología de prototipado evolutivo. Algunos autores afirman que el prototipado evolutivo no debería enmarcarse como una metodología de prototipado, sino como una metodología que ve el sistema como una secuencia de versiones que deben ser evaluadas y que servirán como prototipo para versiones posteriores~\cite{floyd1984systematic}.

El prototipado evolutivo se enfoca principalmente en resolver los problemas de comunicación existentes entre los desarrolladores, que no suelen tener conocimiento sobre el campo de aplicación, y el cliente, que no suele tener una idea clara de sus necesidades y de las posibilidades que pueden ofrecerles los desarrolladores. Por este motivo, y con el fin de conseguir una cooperación entre ambos, se realiza una demostración práctica de las posibles funcionalidades del sistema.

Este modelo se basa en el desarrollo rápido de prototipos de forma que pueda obtenerse una retroalimentación por parte del cliente en poco tiempo. La retroalimentación ayuda al desarrollador a refinar los requisitos del software a desarrollar y a entender mejor qué debe hacerse. También permite ir agregando funcionalidades a lo largo de las iteraciones, por lo que en cada iteración el prototipo irá evolucionando hacia una forma más completa de sí mismo.
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/prototipado-evolutivo.png}
\caption{Flujo del modelo de prototipado evolutivo}
\label{fig:prototipado-evolutivo}
\end{center}
\end{figure}

El prototipado evolutivo comienza con la recogida de unos requisitos básicos que permitan el diseño rápido del primer prototipo a presentar. Tras el diseño de este prototipo, se procede a su construcción, durante la que se realizará un desarrollo rápido que permita presentar el prototipo al cliente en un corto plazo de tiempo. Una vez construido el prototipo, éste será evaluado por el cliente, lo que permitirá una mejor aproximación a sus necesidades, puesto que tras esta evaluación el prototipo podrá ser refinado agregando requisitos o modificando los utilizados para su diseño. Este refinado permitirá el rediseño del prototipo, de forma que una nueva versión podrá ser construida y presentada de nuevo al cliente, repitiendo el proceso hasta que el producto final se ajuste a sus necesidades (ver Figura~\ref{fig:prototipado-evolutivo}).

\section{Herramientas}
En esta sección se presentan todas las herramientas que han sido utilizadas a lo largo de todo el proyecto, tanto hardware como software y una pequeña descripción de su propósito.
\subsection{Medios Hardware}
Los elementos hardware empleados en el desarrollo del proyecto han sido los siguientes:
\begin{itemize}
\item \textbf{Ordenador de sobremesa:}
		Empleado a lo largo de todo el proyecto.
\item \textbf{Ordenador portátil:}
		Empleado en tareas de inspección inalámbrica y pruebas de la red.
\item \textbf{ESP-01:}
		Empleado en tareas de inspección inalámbrica y pruebas de la red.
\item \textbf{Hardware de red elegido en la fase de diseño:}
		Empleado en instalación piloto para realización de pruebas y desarrollo de la aplicación.
\item \textbf{Sensores y hardware \ac{IoT} elegidos en la fase de implementación del sistema:}
		Empleados en desarrollo y pruebas de la aplicación de muestra.
\item \textbf{Raspberry Pi 2 con módulo Wi-Fi \acs{USB}:}
		Empleada en la realización de pruebas.
\item \textbf{Raspberry Pi Zero W:}
		Empleada en la realización de pruebas.
\item \textbf{Smartphone Android:}
		Empleado en la realización de pruebas.
\end{itemize}

\subsection{Medios Software}
Las principales herramientas software utilizadas han sido:
\begin{itemize}
\item \textbf{\LaTeX y editor TexMaker:}
		Empleado en la generación de documentación.
\item \textbf{Microsoft Project:}
		Empleado en la planificación del proyecto.
\item \textbf{StarUML:}
		Empleado en el análisis de requisitos.
\item \textbf{TortoiseHG:}
		Empleado en la gestión del repositorio.
\item \textbf{Acrylic Heat Maps:}
		Empleado en tareas de inspección inalámbrica.
\item \textbf{Acrylic Wifi Professional:}
		Empleado en tareas de inspección inalámbrica.
\item \textbf{Packet Sender:}
		Empleado en tareas de inspección inalámbrica.
\item \textbf{\acs{IDE} Arduino:}
		Empleado en la programación de dispositivos \acs{IoT}.
\item \textbf{Tasmota:}
		Firmware instalado en los dispositivos \ac{IoT}.
\item \textbf{PureLoad:}
		Empleado en las pruebas de la red.
\item \textbf{Kali Linux:}
		Empleado en las pruebas de la red.
\item \textbf{InkScape:}
		Empleado en la creación de imágenes.
\item \textbf{Fritzing:}
		Empleado para crear diagramas de electrónica.
\end{itemize}

% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
