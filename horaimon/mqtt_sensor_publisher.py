#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-
import paho.mqtt.client as mqtt
from datetime import datetime, timedelta
#from app import db, Device, Sensor, Reading

def mqtt_publish(topic, message):
    mqttc = mqtt.Client('horaimon')
    mqttc.username_pw_set('horaimon', password='1234')
    mqttc.connect('localhost', 1883)
    for x in range (1):
        mqttc.publish(topic, message)
    mqttc.loop(1)

if __name__ == "__main__":
    print("horaimon/sonoffs/tele/sonoff2.50/SENSOR", '{"Time":"' + str(datetime.now().strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.9, "Humidity":79.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.50/SENSOR", '{"Time":"' + str(datetime.now().strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.9, "Humidity":79.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.50/SENSOR", '{"Time":"' + str((datetime.now()-timedelta(days=365,hours=1)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.0, "Humidity":78.0}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.50/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=2)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":27.8, "Humidity":78.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.50/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=3)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.5, "Humidity":79.0}, "TempUnit":"C"}')
    #
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.51/SENSOR", '{"Time":"' + str(datetime.now().strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.9, "Humidity":79.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.51/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.0, "Humidity":78.0}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.51/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=2)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":27.8, "Humidity":78.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.51/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=3)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.5, "Humidity":79.0}, "TempUnit":"C"}')
    #
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.52/SENSOR", '{"Time":"' + str(datetime.now().strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.9, "Humidity":79.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.52/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.0, "Humidity":78.0}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.52/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=2)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":27.8, "Humidity":78.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.52/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=3)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.5, "Humidity":79.0}, "TempUnit":"C"}')
    #
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.53/SENSOR", '{"Time":"' + str(datetime.now().strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.9, "Humidity":79.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.53/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.0, "Humidity":78.0}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.53/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=2)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":27.8, "Humidity":78.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.53/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=3)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.5, "Humidity":79.0}, "TempUnit":"C"}')
    #
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.54/SENSOR", '{"Time":"' + str(datetime.now().strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.9, "Humidity":79.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.54/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.0, "Humidity":78.0}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.54/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=2)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":27.8, "Humidity":78.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.54/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=3)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.5, "Humidity":79.0}, "TempUnit":"C"}')
    #
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.55/SENSOR", '{"Time":"' + str(datetime.now().strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.9, "Humidity":79.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.55/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.0, "Humidity":78.0}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.55/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=2)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":27.8, "Humidity":78.5}, "TempUnit":"C"}')
    # mqtt_publish("horaimon/sonoffs/tele/sonoff2.55/SENSOR", '{"Time":"' + str((datetime.now()+timedelta(hours=3)).strftime('%Y-%m-%dT%H:%M:%S')) + '", "AM2301":{"Temperature":28.5, "Humidity":79.0}, "TempUnit":"C"}')
