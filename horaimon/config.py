#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-
import logging
loggingLevel = logging.DEBUG
dbfile = '/var/lib/horaimon/db/horaimon.db'
mqttClient = 'horaimon'
mqttPass = '1234'
mqttServerAddr = 'localhost'
mqttPort = 1883
webServerAddr = '0.0.0.0'
webServerPort = 443
adminPass = "1234"
