#!/usr/bin/env bash
if [[ $EUID > 0 ]]; then
  echo "Please run as root/sudo"
  exit 1
else
  export DEBIAN_FRONTEND=noninteractive
  wget -O - https://repo.saltstack.com/apt/debian/9/amd64/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add -
  echo "deb http://repo.saltstack.com/apt/debian/9/amd64/latest stretch main" >> /etc/apt/sources.list.d/saltstack.list
  apt-get update -q
  ##IPTABLES RULES TO WORKAROUND SALT PUBLISH_PORT BUG
  apt-get install -q -y -o Dpkg::Options::="--force-confnew" iptables-persistent
  cat << EOF > /etc/iptables/rules.v4
# Do not edit. This file is provisioned by salt
*nat
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
-A OUTPUT -p tcp -m tcp --dport 4506 -j DNAT --to-destination :4508
-A OUTPUT -p tcp -m tcp --dport 4505 -j DNAT --to-destination :4507
COMMIT

*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
COMMIT
EOF
  iptables-restore < /etc/iptables/rules.v4
  sysctl -w net.ipv4.conf.all.route_localnet=1
  ##INSTALLING SALT-MINION
  apt-get install -q -y salt-minion
  ##CONFIGURING MINION TO WORK WITH SYNDIC MASTER
  cat << EOF > /etc/salt/minion
master: 161.67.140.15
id: horaimon
user: root
publish_port: 4507
master_port: 4508
EOF
  systemctl restart salt-minion
fi
