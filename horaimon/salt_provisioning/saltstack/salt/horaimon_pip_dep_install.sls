pip_installation:
  pkg.installed:
    - pkgs:
      # Python 2 ``pip`` is required for ``pip.installed`` state to work
      - python-pip
      - python3-pip
    - refresh: True
    - reload_modules: True

flask-sqlalchemy:
  pip.installed:
    - bin_env: '/usr/bin/pip3'
    - require:
      - pkg: pip_installation

flask-login:
  pip.installed:
    - bin_env: '/usr/bin/pip3'
    - require:
      - pkg: pip_installation

flask-bootstrap:
  pip.installed:
    - bin_env: '/usr/bin/pip3'
    - require:
      - pkg: pip_installation

flask-wtf:
  pip.installed:
    - bin_env: '/usr/bin/pip3'
    - require:
      - pkg: pip_installation

flask-session:
  pip.installed:
    - bin_env: '/usr/bin/pip3'
    - require:
      - pkg: pip_installation

pyopenssl:
  pip.installed:
    - bin_env: '/usr/bin/pip3'
    - require:
      - pkg: pip_installation

paho-mqtt:
  pip.installed:
    - name: paho-mqtt == 1.2.3
    - bin_env: '/usr/bin/pip3'
    - require:
      - pkg: pip_installation
