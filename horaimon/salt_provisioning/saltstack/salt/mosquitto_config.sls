mosquitto_conf:
  file.managed:
    - name: /etc/mosquitto/mosquitto.conf
    - require:
      - pkg: install_horaimon
    - user: root
    - group: root
    - mode: 644
    - contents: |
        # Place your local configuration in /etc/mosquitto/conf.d/
        #
        # A full description of the configuration file is at
        # /usr/share/doc/mosquitto/examples/mosquitto.conf.example
        pid_file /var/run/mosquitto.pid
        persistence true
        persistence_location /var/lib/mosquitto/
        log_dest file /var/log/mosquitto/mosquitto.log
        log_type error
        log_type warning
        log_type notice
        #log_type information
        #log_type debug
        log_type subscribe
        log_type unsubscribe
        #log_type websockets
        #log_type all
        connection_messages true
        log_timestamp true
        include_dir /etc/mosquitto/conf.d
        allow_anonymous false
        password_file /etc/mosquitto/passwd

mosquitto_passwd:
  file.managed:
    - name: /etc/mosquitto/passwd
    - contents: |
        Sonoff:$6$akXk4HZo8nzH+x1d$WW+c7KtUuCHjc87pcuR9TINLb7q/EEDQinxnBS7O6m6rQXlau1PWJ+gASTves48YeDPWliAp7cA0ftcHRK12lQ==
        horaimon:$6$8M8PLLWbvWYAD8ZK$v2tXX9jPvHD+TwFeTQErI7uJdcvTwECuKD0HAZMVKAHbsarKnF+w1G17i4SMLjNpDWwt2g10sUbnRiXLLD/sMg==

    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: install_horaimon

mqtt_stop:
  service.dead:
    - name: mosquitto
    - require:
      - file: mosquitto_conf
      - file: mosquitto_passwd

mqtt_start:
  service.running:
    - name: mosquitto
    - require:
      - service: mqtt_stop
