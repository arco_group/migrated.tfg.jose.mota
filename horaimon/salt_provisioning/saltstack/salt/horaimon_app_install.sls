install_horaimon:
  pkg.installed:
    - name: horaimon
    - refresh: True
    - reload: True

start_horaimon:
  cmd.run:
    - name: /etc/init.d/horaimon start
    - require:
      - sls : horaimon_pip_dep_install
      - service: mqtt_start
