include:
  - misc.iptables

# /etc/salt/minion.d/syndic.conf:
#   file.managed:
#   - contents: |
#        master: 161.67.140.15
#        id: horaimon
#        user: root
#        publish_port: 4507 # BUG: ignored!
#        master_port: 4508

set_hostname_horaimon:
  system:
    network.system:
      - enabled: True
      - hostname: horaimon.uclm.es
  host.present:
    - ip: 127.0.0.1
    - name: horaimon.uclm.es

install_wpa_supplicant:
  pkg.installed:
    - name: wpasupplicant

esiot_network_config:
  file.managed:
    - name : /etc/network/interfaces.d/wlan0
    - require:
      - pkg: install_wpa_supplicant
    - contents: |
        # Do not edit. This file is provisioned by salt
        auto wlan0
        iface wlan0 inet dhcp
                wpa-ssid esiot
                wpa-psk 58325b004143469fddaf13135dab0bd8068646e0d087694ffca004717937804e

enable_wlan0:
  network.managed:
    - name: wlan0
    - enabled: True

/etc/iptables/rules.v4:
  file.managed:
    - require:
      - pkg: iptables-persistent
    - contents: |
        # Do not edit. This file is provisioned by salt
        *nat
        :PREROUTING ACCEPT [0:0]
        :INPUT ACCEPT [0:0]
        :OUTPUT ACCEPT [0:0]
        :POSTROUTING ACCEPT [0:0]
        -A OUTPUT -p tcp -m tcp --dport 4506 -j DNAT --to-destination :4508
        -A OUTPUT -p tcp -m tcp --dport 4505 -j DNAT --to-destination :4507
        COMMIT

        *filter
        :INPUT ACCEPT [0:0]
        :FORWARD ACCEPT [0:0]
        :OUTPUT ACCEPT [0:0]
        COMMIT

net.ipv4.conf.all.route_localnet:
  sysctl.present:
    - value: 1

iptables_restart:
  service.running:
    - name: iptables
    - enable: True
    - reload: True
