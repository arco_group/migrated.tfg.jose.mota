function paint(sensor_id)
{
    document.getElementById("sensor-ide-title").innerHTML = "Sensor "+sensor_id;
    console.log(sensor_id);
    // var sensor = document.getElementById("sensor_selector").
    //            options[document.getElementById("sensor_selector").
    //            selectedIndex].value;
    var data_magnitude = sensor_id.substring(0,4)
    //console.log(data_magnitude);
    //var data_magnitude= document.querySelector('[name="req"]:checked').value;
    d3.json("data/" + sensor_id, function(error, data)
    {
        //d3.json("data/" + sensor + "/" + data_magnitude, function(error, data) {
       //chart_gauge(data[0][data_magnitude], data_magnitude);
       chart_gauge(data[0].value, data_magnitude);
       chart(data, data_magnitude);
    });
}

function chart_gauge(data,data_magnitude) {
    document.getElementById("chart-gauge").innerHTML ='';

    if(data_magnitude=="Temp") {
    var name = "Temperatura actual";
    var MinValue = -10;
    var MaxValue = 50;
    var units = 'ºC';
    var colours = ["#33cff5", "#aee87e", "#f9f736",
               "#ffbe2f", "#ff7a2d", "#ff302b"];
    }
    else {
    var name = "Humedad actual";
    var MinValue = 0;
    var MaxValue = 100;
    var units = '%';
    var colours = ["#33ccff","#1ac6ff","DeepSkyBlue ","#00ace6","#0099cc","#0086b3"];//1 colour per section
    }

    var numSections= 6;
    var inputValue= data;
    var value = inputValue-MinValue;

    var gaugeMaxValue = MaxValue-MinValue;

    // données à calculer
    var percentValue = value / gaugeMaxValue;

    ////////////////////////

    var needleClient;

    (function(){
    var barWidth, chart, chartInset, degToRad, repaintGauge,
            height, margin, padRad, percToDeg, percToRad,
            percent, radius, sectionIndx, svg, totalPercent, width,
            valueText, formatValue, k;

    percent = percentValue;

    sectionPerc = 1 / numSections / 2;
    padRad = 0;
    chartInset = 10;

    // Orientation of gauge:
    totalPercent = .75;

    el = d3.select('#chart-gauge');

    margin = {
            top: 45,
            right: 45,
        bottom: -el[0][0].offsetWidth/3,
        left: 45
    };

    width = el[0][0].offsetWidth - margin.left - margin.right;
    height = width;
    radius = Math.min(width, height) / 2;
    barWidth = 75 * width / 300;

        //Utility methods
    percToDeg = function(perc) {
            return perc * 360;
    };

    percToRad = function(perc) {
      return degToRad(percToDeg(perc));
    };

    degToRad = function(deg) {
      return deg * Math.PI / 180;
    };

    // Create SVG element
    svg = el.append('svg').attr('width', width + margin.left + margin.right).attr('height', height + margin.top + margin.bottom);

    // Add layer for the panel
    chart = svg.append('g').attr('transform', "translate(" + ((width) / 2 + margin.left) + ", " + ((height + margin.top) / 2) + ")");

    for (sectionIndx = i = 1, ref = numSections; 1 <= ref ? i <= ref : i >= ref; sectionIndx = 1 <= ref ? ++i : --i) {
            arcStartRad = percToRad(totalPercent);
            arcEndRad = arcStartRad + percToRad(sectionPerc);
            totalPercent += sectionPerc;
            startPadRad = sectionIndx === 0 ? 0 : padRad / 2;
            endPadRad = sectionIndx === numSections ? 0 : padRad / 2;
            arc = d3.svg.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - barWidth).startAngle(arcStartRad + startPadRad).endAngle(arcEndRad - endPadRad);
            chart.append('path').attr('class', "arc chart-color" + sectionIndx).attr('d', arc).style("fill", function(sectionIndx){
        return colours[i-1];
        });
    }

    /////////

        var dataset = [{metric:name, value: value}]

        var texts = svg.selectAll("text")
            .data(dataset)
            .enter();

        texts.append("text")
            .text(function(){
            return dataset[0].metric;
            })
            .attr('id', "Name")
            .attr('transform', "translate(" + ((width + margin.left) / 3) + ", " + ((height + margin.top) / 1.65) + ")")
            .attr("font-size",16)
            .style("fill", "maroon");

    //text for the needle
    valueText = chart.append("text")
            .attr('id', "Value")
            .attr("font-size",16)
            .attr("text-anchor","middle")
            .attr("dy",".5em")
            .style("fill", '#000000');
    formatValue = d3.format('.1%');
    var formatValueText = d3.format('.1f');

        var Needle = (function() {
            //Helper function that returns the `d` value for moving the needle
            var recalcPointerPos = function(perc) {
            var centerX, centerY, leftX, leftY, rightX, rightY, thetaRad, topX, topY;
            thetaRad = percToRad(perc / 2);
            centerX = 0;
            centerY = 0;
            topX = centerX - this.len * Math.cos(thetaRad);
            topY = centerY - this.len * Math.sin(thetaRad);
            leftX = centerX - this.radius * Math.cos(thetaRad - Math.PI / 2);
            leftY = centerY - this.radius * Math.sin(thetaRad - Math.PI / 2);
            rightX = centerX - this.radius * Math.cos(thetaRad + Math.PI / 2);
        rightY = centerY - this.radius * Math.sin(thetaRad + Math.PI / 2);
            return "M " + leftX + " " + leftY + " L " + topX + " " + topY + " L " + rightX + " " + rightY;
        };

        function Needle(el) {
        this.el = el;
        this.len = width / 2.5;
        this.radius = this.len / 8;
        }

        Needle.prototype.render = function() {
        this.el.append('circle').attr('class', 'needle-center').attr('cx', 0).attr('cy', 0).attr('r', this.radius);
        return this.el.append('path').attr('class', 'needle').attr('id', 'client-needle').attr('d', recalcPointerPos.call(this, 0));
     };

            Needle.prototype.moveTo = function(perc) {
            var self,
                oldValue = this.perc || 0;

            this.perc = perc;
            self = this;

        // Reset pointer position
        this.el.transition().delay(100).ease('quad').duration(200).select('.needle').tween('reset-progress', function() {
            return function(percentOfPercent) {
            var progress = (1 - percentOfPercent) * oldValue;

            return d3.select(this).attr('d', recalcPointerPos.call(self, progress));
            };
        });

        this.el.transition().delay(300).ease('bounce').duration(1500).select('.needle').tween('progress', function() {
            return function(percentOfPercent) {
            var progress = percentOfPercent * perc;

            var thetaRad = percToRad(progress / 2);
            var textX = - (self.len + 45) * Math.cos(thetaRad);
            var textY = - (self.len + 45) * Math.sin(thetaRad);

            var aux = (MaxValue-MinValue)*progress+MinValue;
            valueText.text(formatValueText(aux)+units)
                .attr('transform', "translate("+textX+","+textY+")")

            return d3.select(this).attr('d', recalcPointerPos.call(self, progress));
            };
        });

        };

        return Needle;

    })();


    needle = new Needle(chart);
    needle.render();
    needle.moveTo(percent);

    })();
}

function chart(data,data_magnitude) {
    document.getElementById("chart").innerHTML ='';
    // This is the format our dates are in, e.g 23-05-2014 22:15:54
    var timeFormat = d3.time.format('%Y-%m-%d %X');

    var dates = [],
    dateStrings = [],
    temps = [];

    data.forEach(function(d) {
    // Convert date string into JS date, add it to dates array
    //var formattedDate = timeFormat.parse(d.date);
    var formattedDate = timeFormat.parse(d.timestamp);
    formattedDate.setMinutes(0);
    formattedDate.setSeconds(0,0);
    dates.push(formattedDate);

    // Keep array of original date strings
    //dateStrings.push(d.date);
    dateStrings.push(timeFormat(formattedDate));

    temps.push(+d.value);
    });

    var chartWidth = 800,
    chartHeight = 250,
    margin = {
        top: 5,
        right: 25,
        bottom: 20,
        left: 25
    };

    var container = d3.select('#chart');

    var svg = container.append('svg')
    .attr('width', chartWidth)
    .attr('height', chartHeight);

    var defs = svg.append('defs');

    // clipping area
    defs.append('clipPath')
    .attr('id', 'plot-area-clip-path')
    .append('rect')
    .attr({
        x: margin.left,
        y: margin.top,
        width: chartWidth - margin.right - margin.left,
        height: chartHeight - margin.top - margin.bottom
    });

    // Invisible background rect to capture all zoom events
    var backRect = svg.append('rect')
    .style('stroke', 'none')
    .style('fill', '#FFF')
    .style('fill-opacity', 0)
    .attr({
        x: margin.left,
        y: margin.top,
        width: chartWidth - margin.right - margin.left,
        height: chartHeight - margin.top - margin.bottom,
        'pointer-events': 'all'
    });

    var axes = svg.append('g')
    .attr('pointer-events', 'none')
    .style('font-size', '11px');

    var chart = svg.append('g')
    .attr('class', 'plot-area')
    .attr('pointer-events', 'none')
    .attr('clip-path', 'url(#plot-area-clip-path)');

    // x scale
    var xScale = d3.time.scale()
    .range([margin.left, chartWidth - margin.right])
    .domain(d3.extent(dates));

    // Calculate the range of the temperature data
    var yExtent = d3.extent(temps);
    var yRange = yExtent[1] - yExtent[0];

    // Adjust the lower and upper bounds to force the data
    // to fit into the y limits nicely
    yExtent[0] = yExtent[0] - yRange * 0.1;
    yExtent[1] = yExtent[1] + yRange * 0.1;

    // the y scale
    var yScale = d3.scale.linear()
    .range([chartHeight - margin.bottom, margin.top])
    .domain(yExtent);

    // x axis
    var xAxis = d3.svg.axis()
    .orient('bottom')
    .outerTickSize(0)
    .innerTickSize(0)
    .scale(xScale)
    .tickFormat(d3.time.format("%H:%M"));//revisar

    // y axis
    var yAxis = d3.svg.axis()
    .orient('left')
    .outerTickSize(0)
    .innerTickSize(- (chartWidth - margin.left - margin.right))  // trick for creating quick gridlines
    .tickFormat(d3.format(',.1f'))
    .scale(yScale);

    var yAxis2 = d3.svg.axis()
    .orient('right')
    .outerTickSize(0)
    .innerTickSize(0)
    .tickFormat(d3.format(',.1f'))
    .scale(yScale);

    // Add the x axis to the chart
    var xAxisEl = axes.append('g')
    .attr('class', 'x-axis')
    .attr('transform', 'translate(' + 0 + ',' + (chartHeight - margin.bottom) + ')')
    .call(xAxis);

    // Add the y axis to the chart
    var yAxisEl = axes.append('g')
    .attr('class', 'y-axis')
    .attr('transform', 'translate(' + margin.left + ',' + 0 + ')')
    .call(yAxis);

    // Add the y axis to the chart
    var yAxisEl2 = axes.append('g')
    .attr('class', 'y-axis right')
    .attr('transform', 'translate(' + (chartWidth - margin.right) + ',' + 0 + ')')
    .call(yAxis2);

    // Format y-axis gridlines
    yAxisEl.selectAll('line')
    .style('stroke', '#BBB')
    .style('stroke-width', '1px')
    .style('shape-rendering', 'crispEdges');


    // Start data as a flat line at the average
    var avgTempY = yScale(d3.mean(temps));

    // Path generator function for our data
    var pathGenerator = d3.svg.line()
    .x(function(d, i) { return xScale(dates[i]); })
    .y(function(d, i) { return yScale(temps[i]); });

    // Series container element
    var series = chart.append('g');

    // Add the temperature series path to the chart
    series.append('path')
    .attr('vector-effect', 'non-scaling-stroke')
    .style('fill', 'none')
    //.style('stroke', 'red')//color de linea
    .style('stroke-width', '1px')
    .attr('d', pathGenerator(dates));
    if (data_magnitude == "Temp")
    {
    series.style('stroke', 'red');
    }
    else
    {
    series.style('stroke', 'DeepSkyBlue');
    }


    // Add zooming and panning functionality, only along the x axis
    var zoom = d3.behavior.zoom()
    .scaleExtent([1, 12])
    .x(xScale)
    .on('zoom', function zoomHandler() {

        axes.select('.x-axis')
        .call(xAxis);

        series.attr('transform', 'translate(' + d3.event.translate[0] + ',0) scale(' + d3.event.scale + ',1)');

    });

    // The backRect captures zoom/pan events
    backRect.call(zoom);


    // Function for resetting any scaling and translation applied
    // during zooming and panning. Returns chart to original state.
    function resetZoom() {

    zoom.scale(1);
    zoom.translate([0, 0]);

    // Set x scale domain to the full data range
    xScale.domain(d3.extent(dates));

    // Update the x axis elements to match
    axes.select('.x-axis')
        .transition()
        .call(xAxis);

    // Remove any transformations applied to series elements
    series.transition()
        .attr('transform', "translate(0,0) scale(1,1)");

    };

    // Call resetZoom function when the button is clicked
    d3.select("#reset-zoom").on("click", resetZoom);

    // Active point element
    var activePoint = svg.append('circle')
    .attr({
        cx: 0,
        cy: 0,
        r: 5,
        'pointer-events': 'none'
    });

    if (data_magnitude == "Temp") {
    activePoint.style({
        stroke: 'none',
        fill: 'red',
        'fill-opacity': 0
    });
    }
    else {
    activePoint.style({
        stroke: 'none',
        fill: 'DeepSkyBlue',
        'fill-opacity': 0
    });
    }


    // Set container to have relative positioning. This allows us to easily
    // position the tooltip element with absolute positioning.
    container.style('position', 'relative');

    // Create the tooltip element. Hidden initially.
    var tt = container.append('div')
    .style({padding: '5px',
        border: '1px solid #AAA',
        color: 'black',
        position: 'absolute',
        visibility: 'hidden',
        'background-color': '#F5F5F5'
           });

    // Function for hiding the tooltip
    function hideTooltip() {
    tt.style('visibility', 'hidden');
    activePoint.style('fill-opacity', 0);
    }

    function showTooltip() {
    tt.style('visibility', 'visible');
    activePoint.style('fill-opacity', 1);
    }

    // Tooltip content formatting function
    function tooltipFormatter(date, temp) {
    var dateFormat = d3.time.format('%d %b %Y , %H:%M');
    var unit = (data_magnitude == "Temp") ? '°C':'%' ;
    return dateFormat(date) + '<br><b>' + temp.toFixed(1) + unit;
    }

    backRect.on('mousemove', function() {
    // Coords of mousemove event relative to the container div
    var coords = d3.mouse(container.node());

    // Value on the x scale corresponding to this location
    var xVal = xScale.invert(coords[0]);

    // Date object corresponding the this x value. Add 12 hours to
    // the value, so that each point occurs at midday on the given date.
    // This means date changes occur exactly halfway between points.
    // This is what we want - we want our tooltip to display data for the
    // closest point to the mouse cursor.
    var d = new Date(xVal.getTime());//+ 3600000 * 12);
    d.setMinutes(0);//GRANULARIDAD DE LAS LECTURAS: PARA QUE COINCIDAN CON LA HORA EN PUNTO!
    d.setSeconds(0,0);
    //document.getElementById("debug").innerHTML = d;
        // Format the date object as a date string matching our original data
    var date = timeFormat(d);

    // Find the index of this date in the array of original date strings
    var i = dateStrings.indexOf(date);

    // Does this date exist in the original data?
    var dateExists = i > -1;

    // If not, hide the tooltip and return from this function
    if (!dateExists) {
        hideTooltip();
        return;
    }

    // If we are here, the date was found in the original data.
    // Proceed with displaying tooltip for of the i-th data point.

    // Get the i-th date value and temperature value.
    var _date = dates[i],
        _temp = temps[i];

    // Update the position of the activePoint element
    activePoint.attr({
        cx: xScale(_date),
        cy: yScale(_temp)
    });

    // Update tooltip content
    tt.html(tooltipFormatter(_date, _temp));

    // Get dimensions of tooltip element
    var dim = tt.node().getBoundingClientRect();

    // Update the position of the tooltip. By default, above and to the right
    // of the mouse cursor.
    var tt_top = coords[1] - dim.height - 10,
        tt_left = coords[0] + 10;

    // If right edge of tooltip goes beyond chart container, force it to move
    // to the left of the mouse cursor.
    if (tt_left + dim.width > chartWidth)
        tt_left = coords[0] - dim.width - 10;

    tt.style({
        top: tt_top + 'px',
        left: tt_left + 'px'
    });

    // Show tooltip if it is not already visible
    if (tt.style('visibility') != 'visible')
        showTooltip();
    });

    // Add mouseout event handler
    backRect.on('mouseout', hideTooltip);
}
