function getLastEdited(default_name, csrf_token)
{
    console.log(csrf_token)
    var url = window.location.href;
    var name = url.split('=')[1];
    if (name == null)
    {
        loadDeviceData(default_name, csrf_token);
    }
    else
    {
        alert("Editado "+name);
        if(name.substring(0, 6) == 'sonoff')
        {
            loadDeviceData(name, csrf_token);
        }
        else
        {
            loadSensorData(name, csrf_token);
        }
    }
}

function loadDeviceData(device_name, csrf_token)
{
    document.getElementById("uneditable").innerHTML ='';
    loadDeviceForm(device_name,true,csrf_token);
}

function loadSensorData(sensor_name, csrf_token)
{
    loadSensorForm(sensor_name,csrf_token);
}

function loadDeviceForm(device_name, editable, csrf_token) {
    var json_obj = JSON.parse(Get("/metadata/device/"+device_name));

    if(editable)
        document.getElementById("editable").innerHTML=
            '<h1 class="page-header" style="margin-top:0px;">Device</h1>'+
            '<form action="/metadata/add/device" method="post" name="devForm" id="devForm" style="margin: 10px 50px 10px 50px;">'+
              '<fieldset enabled>'+
                '<div class="navbar-left" style="margin-right: 30px;">'+
                    '<div class="form-group">'+
                        'Name: '+
                        '<input type="text" name="dname" class="form-control" value="'+json_obj.name+'" readonly>'+
                    '</div>'+
                    '<div class="form-group">'+
                        'Brand: '+
                        '<input type="text" name="dbrand" class="form-control" value="'+json_obj.brand+'" placeholder="ex: Sonoff">'+
                    '</div>'+
                '</div>'+
                '<div class="navbar-left" style="margin-right: 30px;">'+
                    '<div class="form-group">'+
                        'Location: '+
                        '<input type="text" name="dlocation" class="form-control" value="'+json_obj.location+'" placeholder="ex: Despacho 2.08">'+
                    '</div>'+
                    '<div class="form-group">'+
                        'Model: '+
                        '<input type="text" name="dmodel" class="form-control" value="'+json_obj.model+'" placeholder="ex: TH10">  '+
                    '</div>'+
                '</div>'+
                '<input type="hidden" name="csrf_token" value="'+csrf_token+'"/>'+
              '</fieldset>'+
            '<input type="submit" form="devForm" value="Submit" class="btn btn-primary">'+
            '</form>';
    else
        document.getElementById("uneditable").innerHTML=
            '<h1 class="page-header" style="margin-top:0px;">Device</h1>'+
            '<form action="/metadata/add/device" method="post" name="devForm" id="devForm" style="margin: 10px 50px 10px 50px;">'+
              '<fieldset disabled>'+
                '<div class="navbar-left" style="margin-right: 30px;">'+
                    '<div class="form-group">'+
                        'Name: '+
                        '<input type="text" name="dname" class="form-control" value="'+json_obj.name+'" readonly>'+
                    '</div>'+
                    '<div class="form-group">'+
                        'Brand: '+
                        '<input type="text" name="dbrand" class="form-control" value="'+json_obj.brand+'" readonly>'+
                    '</div>'+
                '</div>'+
                '<div class="navbar-left" style="margin-right: 30px;">'+
                    '<div class="form-group">'+
                        'Location: '+
                        '<input type="text" name="dlocation" class="form-control" value="'+json_obj.location+'" readonly>'+
                    '</div>'+
                    '<div class="form-group">'+
                        'Model: '+
                        '<input type="text" name="dmodel" class="form-control" value="'+json_obj.model+'" readonly>  '+
                    '</div>'+
                '</div>'+
              '</fieldset>'+
            '</form>';
}

function loadSensorForm(sensor_name,csrf_token) {
    var json_obj = JSON.parse(Get("/metadata/sensor/"+sensor_name));
    loadDeviceForm(json_obj.metadata[0].name,false,csrf_token)
    document.getElementById("editable").innerHTML=
    '<h1 class="page-header">Sensor</h1>'+
    '<form action="/metadata/add/sensor" method="post" name="sensForm" id="sensForm" style="margin: 10px 50px 10px 50px;">'+
       '<div class="navbar-left" style="margin-right: 30px;">'+
           '<div class="form-group">'+
               'Name: '+
               '<input type="text" name="sname" class="form-control" value="'+json_obj.metadata[1].name+'" readonly>'+
           '</div>'+
           '<div class="form-group">'+
               'Units: '+
               '<input type="text" name="sunits" class="form-control" value="'+json_obj.metadata[1].units+'">'+
           '</div>'+
           '<input type="hidden" name="csrf_token" value="'+csrf_token+'"/>'+
           '<input type="submit" form="sensForm" value="Submit" class="btn btn-primary">'+
       '</div>'+
       '<div class="navbar-left" style="margin-right: 30px;">'+
           '<div class="form-group">'+
               'Type: '+
               '<input type="text" name="stype" class="form-control" value="'+json_obj.metadata[1].type+'" readonly>'+
           '</div>'+
           '</br>'+
           '<div class="form-group">'+
               '<div class="input-group ">'+
                   '<div class="input-group-addon">MIN</div>'+
                   '<input type="number" name="smin" class="form-control" value="'+json_obj.metadata[1].min+'">'+
               '</div>'+
               '</br>'+
               '<div class="input-group">'+
                   '<div class="input-group-addon">MAX</div>'+
                   '<input type="number" name="smax" class="form-control" value="'+json_obj.metadata[1].max+'">'+
                '</div>'+
           '</div>'+
       '</div>'+
    '</form>';


    /*'Tipo: <input type="text" name="stype" value="'+json_obj.metadata[1].type+'" readonly>  '+
    'Unidades: <input type="text" name="sunits" value="'+json_obj.metadata[1].units+'" size="5">  '+
    'Max: <input type="text" name="smax" value="'+json_obj.metadata[1].max+'" size="5">  '+
    'Min: <input type="text" name="smin" value="'+json_obj.metadata[1].min+'" size="5">  '+
    '</form>';*/
}

function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;
}
