#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

import hamcrest
from prego import Task, TestCase, running
from prego.net import localhost, listen_port, Host
from prego.debian import Package, installed


class Horaimon(TestCase):
    def test_simple(self):
        server = Task(desc='horaimon', detach=True)
        server.assert_that(Package('mosquitto'), installed())
        server.assert_that(Host('0.0.0.0'), listen_port(1883))
        cmd = server.command('./horaimon.py')

        client = Task(desc='publisher')
        client.wait_that(server, running())
        client.wait_that(localhost, listen_port(8000))
        client.command('./mqtt_sensor_publisher.py')
        client.wait_that(cmd.stderr.content,
                         hamcrest.contains_string("INFO:root:Sensor 1 event"))
