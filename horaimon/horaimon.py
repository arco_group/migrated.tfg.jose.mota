#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import json
from flask import Flask, render_template, render_template_string, jsonify, request, redirect, url_for, flash, current_app, session, abort
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, UserMixin, login_required, LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from flask_wtf.csrf import CSRFProtect
from wtforms import StringField, PasswordField, BooleanField, SubmitField, HiddenField
from wtforms.validators import Required, Length

import config as conf
import logging
logging.basicConfig(level=conf.loggingLevel)

import paho.mqtt.client as mqtt
import re
import fileinput
import os

from OpenSSL import SSL

dbfile = conf.dbfile

app = Flask(__name__, static_folder='assets')

csrf = CSRFProtect(app)
bootstrap = Bootstrap(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + dbfile
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.url_map.strict_slashes = False
db = SQLAlchemy(app)

def replaceFieldInFile(fileToSearch, fieldName, password):
    tempFile = open(fileToSearch, 'r+')
    for line in fileinput.input(fileToSearch, inplace=True):
        if fieldName in line:
            print(fieldName + " = \'" + password + "\'")
        else:
            print(line, end='')
    tempFile.close()

def make_fixtures (username, password):
    db.session.add(User(username, password))
    db.session.commit()

class User(UserMixin, db.Model):
    __tablename__ = 'user'
    id = db.Column (db.Integer, primary_key = True)
    username = db.Column (db.String(64), unique = True, nullable = False)
    password_hash = db.Column(db.String(128))
    firstLogin = db.Column(db.Boolean)

    def __init__(self, username, password):
        self.username = username
        self.password(password)
        self.firstLogin = True

    def password(self):
        raise AttributeError('password no es un atributo legible')

    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

class LoginForm(FlaskForm):
    username = StringField('Usuario', validators = [Required(), Length(1, 64)])
    password = PasswordField('Contraseña', validators = [Required()])
    remember_me = BooleanField('Manténgame registrado')
    submit = SubmitField('Acceder')

class passwdChangeForm(FlaskForm):
    adminPass = PasswordField('Contraseña de Admin', validators = [Required()])
    mqttPass = PasswordField('Contraseña de cliente MQTT para Horaimon', validators = [Required()])
    mqttPassSonoff = PasswordField('Contraseña de  cliente MQTT para Sonoffs', validators = [Required()])
    SonoffWebUIPass = PasswordField('Contraseña para la interfaz web individual de cada Sonoff', validators = [Required()])
    submit = SubmitField('Modificar datos')

class Device(db.Model):
    __tablename__ = 'device'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String, unique = True, nullable = False)
    brand = db.Column(db.String)
    model = db.Column(db.String)
    location = db.Column(db.String)
    sensors = db.relationship('Sensor', backref = 'device', lazy = 'dynamic')

    def __init__(self, name):
        self.name = name

    def toJSON(self):
        return {
            'id': self.id,
            'name': self.name,
            'brand': self.brand,
            'model': self.model,
            'location': self.location,
        }

class Sensor(db.Model):
    __tablename__ = 'sensor'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    type = db.Column(db.String)
    units = db.Column(db.String)
    max = db.Column(db.Float)
    min = db.Column(db.Float)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
    readings = db.relationship('Reading', backref='sensor', lazy='dynamic')

    def __init__(self, name, type, device_id, units):
        self.name = name
        self.type = type
        self.device_id = device_id
        self.units = units

    def toJSON(self):
        return {
            'id': self.id,
            'name': self.name,
            'type': self.type,
            'units': self.units,
            'max': self.max,
            'min': self.min,
            'device_id': self.device_id,
        }

class Reading(db.Model):
    __tablename__ = 'reading'
    timestamp = db.Column(db.TIMESTAMP(timezone=True), primary_key=True)
    value = db.Column(db.Float)
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'), primary_key=True)

    def __init__(self, timestamp, value, sensor_id):
        self.timestamp = timestamp
        self.value = value
        self.sensor_id = sensor_id

def datetime_serialize(obj):
    """JSON serializer for objects not serializable by default json code"""
    if not isinstance(obj, datetime):
        raise TypeError("Type not serializable")

    return obj.strftime('%Y-%m-%d %H:%M:%S')

###### MQTT CLIENT #######
def mqtt_publish(topic, message):
    client = mqtt.Client(conf.mqttClient+'pub')
    client.username_pw_set(conf.mqttClient, password=conf.mqttPass)
    client.connect(conf.mqttServerAddr, conf.mqttPort, 60)
    client.publish(topic, message)
    client.disconnect()

def mqtt_subscribe(topic):
    client = mqtt.Client(conf.mqttClient+'sub')
    client.username_pw_set(conf.mqttClient, password=conf.mqttPass)

    def on_connect(client, userdata, rc):
        client.subscribe(topic)

    def on_message(client, userdata, msg):
        print("Topic: " + msg.topic + ", Message: " + str(msg.payload))
        record_in_db(msg.topic, str(msg.payload))

    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(conf.mqttServerAddr, conf.mqttPort, 60)
    client.loop_start()

def record_in_db(topic, message):
    topiclist = topic.split("/")
    name = topiclist[len(topiclist)-2]
    queryResult = db.session.query(Device).filter(Device.name == name).all()
    reading = json.loads(message[2:-1])
    if not queryResult:
        units = reading['TempUnit']
        register_new_device(name, units)
    deviceID = db.session.query(Device).filter(Device.name == name).all()[0].id
    timestamp = reading['Time']
    timestamp = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S')
    temperature = reading['AM2301']['Temperature']
    humidity = reading['AM2301']['Humidity']
    tempSensorID = db.session.query(Sensor).filter(Sensor.device_id == deviceID).\
    filter(Sensor.type == 'Temperature').all()[0].id
    humSensorID = db.session.query(Sensor).filter(Sensor.device_id == deviceID).\
    filter(Sensor.type == 'Humidity').all()[0].id
    db.session.add(Reading(timestamp, temperature, tempSensorID))
    db.session.add(Reading(timestamp, humidity, humSensorID))
    db.session.commit()
    logging.info("Sensor {} event".format(tempSensorID))


def register_new_device(name, units):
    db.session.add(Device(name))
    db.session.commit()
    deviceID = db.session.query(Device).filter(Device.name == name).all()[0].id
    tempSensorName="Temp"+name[-4:]
    humSensorName="Humi"+name[-4:]
    db.session.add(Sensor(tempSensorName,'Temperature',deviceID, units))
    db.session.add(Sensor(humSensorName,'Humidity',deviceID, '%'))
    db.session.commit()
#########################

@app.route('/login', methods=['GET','POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = db.session.query(User).filter(User.username == form.username.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            flash('Bienvenido.')
            print("First Login = " + str(user.firstLogin))
            if user.firstLogin:
                flash('Debe cambiar las contraseñas.')
                return redirect(url_for('passwdChange'))
            else:
                return redirect(url_for('main'))
        else:
            flash('Usuario o contraseña no válido.')
    return render_template('login.html',form = form), 200

@app.route('/passwdChange', methods=['GET','POST'])
@login_required
def passwdChange():
    fileToSearch = '/usr/lib/horaimon/config.py'
    SonoffMQTTUser = 'Sonoff'
    flash('La aplicación se reiniciará tras modificar las contraseñas.')

    form = passwdChangeForm()
    if form.validate_on_submit():
        adminPass = form.adminPass.data
        user = db.session.query(User).filter(User.username == 'admin').first()
        user.password(adminPass)
        user.firstLogin = False
        db.session.commit()

        SonoffWebUIPass = form.SonoffWebUIPass.data
        mqtt_publish('horaimon/sonoffs/cmnd/WebPassword', SonoffWebUIPass)

        mqttPassSonoff = form.mqttPassSonoff.data
        mqtt_publish('horaimon/sonoffs/cmnd/MqttPassword', mqttPassSonoff)
        bashCommand = 'mosquitto_passwd -b /etc/mosquitto/passwd '+ SonoffMQTTUser + ' ' + mqttPassSonoff
        os.system(bashCommand)

        mqttPass = request.form['mqttPass']
        replaceFieldInFile(fileToSearch, 'mqttPass', mqttPass)
        bashCommand = 'mosquitto_passwd -b /etc/mosquitto/passwd '+ conf.mqttClient + ' ' + mqttPass
        os.system(bashCommand)

        bashCommand = 'service mosquitto restart'
        os.system(bashCommand)
        bashCommand = '/etc/init.d/horaimon restart'
        os.system(bashCommand)
    return render_template('passwdchange.html', form = form), 200

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('main'))

@app.route('/data/<sensor_name>')
@login_required
def data(sensor_name):
    readings = db.session.query(Device, Sensor, Reading).filter(Sensor.name == sensor_name).\
    filter(Sensor.id == Reading.sensor_id).filter(Reading.timestamp > datetime.now()-timedelta(days=365)).\
    order_by(Reading.timestamp.desc()).values(Reading.timestamp,Reading.value)

    return json.dumps([x._asdict() for x in readings], default=datetime_serialize)

@app.route('/metadata/device/<device_name>')
@login_required
def devMeta(device_name):
    device = db.session.query(Device).filter(Device.name == device_name).all()[0]
    return json.dumps(device.toJSON())

@app.route('/metadata/sensor/<sensor_name>')
@login_required
def sensMeta(sensor_name):
    result = db.session.query(Device,Sensor).filter(Sensor.name == sensor_name).\
    filter(Device.id == Sensor.device_id).all()
    list = [result[0].Device.toJSON(),result[0].Sensor.toJSON()]
    return jsonify(metadata = list)

@app.route('/metadata/add/sensor', methods=['POST'])
@login_required
def sensAdd():
    name = request.form['sname']
    units = request.form['sunits']
    min = request.form['smin']
    max = request.form['smax']
    print("min= "+min+", max= "+max)
    sensor = Sensor.query.filter_by(name = name).first()
    sensor.units = None if units == '' else units
    sensor.min = None if min == '' else float(min)
    sensor.max = None if max == '' else float(max)
    print("min= "+str(sensor.min)+", max= "+str(sensor.max))
    db.session.commit()
    return redirect(url_for("editor", last_edited = name))

@app.route('/metadata/add/device', methods=['POST'])
@login_required
def devAdd():
    name = request.form['dname']
    location = request.form['dlocation']
    brand = request.form['dbrand']
    model = request.form['dmodel']
    device = Device.query.filter_by(name = name).first()
    device.location = None if location == '' else location
    device.brand = None if brand == '' else brand
    device.model = None if model == '' else model
    db.session.commit()
    return redirect(url_for("editor", last_edited = name))

@app.route('/')
@login_required
def main():
    sensors = db.session.query(Device, Sensor).distinct(Device.name).\
    filter(Device.name.ilike("Sonoff%")).filter(Device.id == Sensor.device_id).\
    order_by(Device.name).values(Sensor.name)
    sensor_names = [s.name for s in sensors]
    return render_template('main.html',sensors=sensor_names), 200


@app.route('/editor', methods=['GET'])
@login_required
def editor():
    devices = db.session.query(Device, Sensor).distinct(Device.name).\
    filter(Device.name.ilike("Sonoff%")).order_by(Device.name).values(Device.name)
    device_names = [d.name for d in devices]

    sensors = db.session.query(Device, Sensor).distinct(Device.name).\
    filter(Device.name.ilike("Sonoff%")).filter(Device.id == Sensor.device_id).\
    order_by(Device.name).values(Sensor.name)
    sensor_names = [s.name for s in sensors]

    return render_template('editor.html',sensors=sensor_names, devices=device_names), 200

if __name__ == "__main__":
    #db.drop_all()
    db.create_all()
    if db.session.query(User).filter(User.username == "admin").scalar() is None:
        make_fixtures("admin",conf.adminPass)

    login_manager = LoginManager()
    login_manager.session_protection = 'strong'
    login_manager.login_view = '/login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    app.secret_key = os.urandom(24)

    mqtt_subscribe("horaimon/sonoffs/tele/+/SENSOR")

    app.run(host=conf.webServerAddr, port=conf.webServerPort, debug = False, ssl_context = 'adhoc')
