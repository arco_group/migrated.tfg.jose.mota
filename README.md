#DISEÑO Y DESPLIEGUE DE UNA RED INALÁMBRICA DE I+D PARA APLICACIONES RELACIONADAS CON EL INTERNET DE LAS COSAS#

## Propósito ##
Este repositorio está dedicado a recopilar documentación y código generados durante el estudio, diseño y despliegue de la red desplegada en la **Escuela Superior de Informática** de Ciudad Real.

## Estructura y contenidos ##
La estructura de este repositorio es la siguiente:
### Ante ###
Documento en *LaTeX* del anteproyecto presentado.
### Doc ###
Documentación en *LaTeX* generada a lo largo del desarrollo del proyecto para la aprobación por parte del cliente.
### Memoria ###
Documento en *LaTeX* que contiene la memoria del Trabajo de Fin de Grado presentada al tribunal.
### Wi-Fi_Quality_Meter ###
Código y esquema del dispositivo que se ha diseñado para estimar la calidad de la señal Wi-Fi.
### ESI-Dashboard ###
Código de la aplicación web desarrollada durante este proyecto.
Esta aplicación consiste en un cliente MQTT que se subscribe a un topic específico para recibir y almacenar las lecturas enviadas por los sensores desplegados. Estos datos son mostrados a través de una interfaz web en forma de gráficas interactivas. Además, también se proporciona un panel de configuración en el que obtener y modificar los datos de los diferentes dispositivos registrados.