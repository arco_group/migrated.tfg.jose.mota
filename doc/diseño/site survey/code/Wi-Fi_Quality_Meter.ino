/*
 * WI-FI Quality Meter
 * Author: Jose Ignacio Mota Ortiz
 * Description: Connects to given SSID, measures signal strength and 
 * lights RGB LED depending on it.
 * Also sends 10 UDP packets to server running PacketSender, waiting 0.5s
 * for each. After that timeout packets are considered lost and after 10 packets
 * are sent, percentage of received packets is shown.
 */
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define RED 0
#define GREEN 2
#define TIMEOUT 500

const char* ssid = "ESI_IoT"; //Input your AP's SSID here
const char* password = ""; //Input your network password here
const IPAddress ip(192,168,1,159); //Server responding UDP packets
const int remoteUdpPort = 4210; //PacketSender default port

WiFiUDP Udp;
unsigned int localUdpPort = 4210;  // local port to listen on
char incomingPacket[255];  // buffer for incoming packets
char outgoingPacket[] = "testESP here,testing with a relatively long string 
which makes absolutely no sense.";

void setup()
{
  //outputs for the RGB LED, we'll only need red and green
  pinMode(RED,OUTPUT);
  pinMode(GREEN,OUTPUT);
  
  Serial.begin(115200);
  Serial.println();

  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected");

  Udp.begin(localUdpPort);
  Serial.printf("Listening at IP %s, UDP port %d\n",
  WiFi.localIP().toString().c_str(), localUdpPort);
}

void loop() {
  long rssi = WiFi.RSSI();  
  int percentage = 0;
  changeLEDColor(rssi);
  Serial.printf("%s, Ch:%d (%ddBm), \n", WiFi.SSID().c_str(),
  WiFi.channel(), rssi);
  percentage = packetLoss(ip,remoteUdpPort);
  Serial.printf("%i%% Packets Received\n",percentage);
}

int packetLoss(IPAddress ip, int remoteUdpPort)
{
  int percentage = 0, packetSize;
  long lastPacketSentMillis;
  
  //send 10 packets waiting 0.5s for each one
  for(int i=0; i<10; i++)
  {
    //Serial.printf("Sending packet #%i to: %s:%d\n", i+1,
    //ip.toString().c_str(), remoteUdpPort);
    Udp.beginPacket(ip,remoteUdpPort);
    Udp.write(outgoingPacket);
    Udp.endPacket();
    lastPacketSentMillis = millis();
    while (packetSize = Udp.parsePacket(), !packetSize && 
    (millis() - lastPacketSentMillis < TIMEOUT))
    {}
    
    if(packetSize)
    {
      //Serial.println("RESPONSE RECEIVED");
      int len = Udp.read(incomingPacket, 255);
      if (len > 0)
      {
        incomingPacket[len] = 0;
      }
      //Serial.printf("UDP packet contents: %s\n", incomingPacket);
      percentage+=10;
    }
    else
    {
      //Serial.println("TIMED OUT.");
    }
  }
  return percentage;
}

void changeLEDColor(long rssi)
{
  int redValue = 0, greenValue = 0;
  if(rssi >= -25)
  {
    greenValue = 1023;
  }
  else if (rssi < -25 && rssi >= -50)
  {
    greenValue = 1023;
    redValue = 10.23 * ((rssi * -4) - 100);    
  }
  else if (rssi < -50 && rssi >= -75)
  {
    greenValue = 10.23 * ((rssi * 4) + 300);
    redValue = 1023;
  }
  else if (rssi < -75)
  {
    redValue = 1023;
  }
  analogWrite(GREEN, greenValue);
  analogWrite(RED, redValue);  
}

